const i18n = {
  'en-US': {
    'group.basic.secretKey':'Secret Key',
  },
  'zh-CN': {
    'group.basic.secretKey':'统计组秘钥',
  },
};

export default i18n;
